import math

# Given n, take the sum of the digits of n. If that value has more than one
# digit, continue reducing in this way until a single-digit number is produced.
# The input will be a non-negative integer.

def digital_root(n):
    list = str(n)
    cont = 0
    while len(list) > 1:
        cont = 0
        for n in range(len(list)):
            cont = cont + int(list[n])
        list = str(cont)
    return cont

# Given an array of integers, find the one that appears an odd number of times.
#  There will always be only one integer that appears an odd number of times.

# In this little assignment you are given a string of space separated numbers,
# and have to return the highest and lowest number.

def high_and_low(numbers):
    number = [int(x) for x in numbers.split()]
    return max(number), min(number)

# Make a program that filters a list of strings and returns a list with only your
# friends name in it.
#
# If a name has exactly 4 letters in it, you can be sure that it has to be a friend
# of yours! Otherwise, you can be sure he's not...

def friend(x):
    shouldBe=[]
    for n in x:
        if len(n) == 4:
            shouldBe.append(n)
    print(shouldBe)

# An isogram is a word that has no repeating letters, consecutive or non-consecutive.
# Implement a function that determines whether a string that contains only letters is an isogram.
# Assume the empty string is an isogram. Ignore letter case

def is_isogram(palabra):
    pal = palabra.lower()
    for n in pal:
        if pal.count(n) > 1:
            return False
        return True


# You are given an array (which will have a length of at least 3, but could be very large)
# containing integers. The array is either entirely comprised of odd integers or entirely comprised
# of even integers except for a single integer N. Write a method that takes the array as an argument
# and returns this "outlier" N.

def find_outlier(list):
    if list[0] % 2 == 1 and list[1] % 2 == 1:
        for n in list[2::]:
            if n % 2 == 0:
                return n
    elif list[0] % 2 == 0 and list[1] % 2 == 0:
        for n in list[2::]:
            if n % 2 == 1:
                return n

# Create a function that returns the sum of the two lowest positive numbers given an array of minimum
# 4 positive integers. No floats or non-positive integers will be passed.


def sum_two_smallest_numbers(num_list):
    num_list.sort()
    return num_list[0] + num_list[1]


# A Narcissistic Number is a positive number which is the sum of its own digits, each raised to the
# power of the number of digits in a given base. In this Kata, we will restrict ourselves to decimal (base 10).



def narcissistic( value ):
    div = len(str(value))
    sum = 0
    for x in str(value):
        sum+=math.pow(int(x), div)
    if sum == value:
        return True
    else:
        return False


