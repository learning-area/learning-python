# learning-python

Repository containing python code practise. 

## Plan
### Intro:
#### 1 June:

- [x] History and Philosophy of Python
- [x] The Interpreter, an Interactive Shell
- [x] The Interpreter, an Interactive Shell
- [x] Execute a Script
- [x] Structuring with Indentation
- [x] Assignment Expressions  
- ---------------------------------
#### 2 June:
- [x] Data Types and Variables
- [x] Operators
- [x] Sequential Data Types (Slicing)
- ---------------------------------
- [ ] Sequential Data Types 
- [ ] List Manipulation
- [ ] Shallow and Deep Copy
- [ ] Dictionaries
- [ ] Sets and Frozen Sets
- [ ] Sets Examples
- [ ] Keyboard Input
- [ ] Conditional Statements
- [ ] Structural Pattern Matching
- [ ] While Loops
- [ ] For Loops
- [ ] Output with Print
- [ ] Formatted Output
- [ ] Working with Dictionaries and while Loops
- [ ] Functions
- [ ] Passing Arguments
- [ ] Namespaces
- [ ] Global vs. Local Variables and Namespaces
- [ ] File Management
- [ ] Modular Programming and Modules
- [ ] Packages
- [ ] Errors and Exception Handling